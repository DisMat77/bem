window.onload = function(){
    /*open form*/
    const openForm = document.querySelector('.top__desktop-more');
    const form = document.querySelector('.top__form');
    const arrow = document.querySelector('.top__desktop-more');
    let flagForm = false;

    function showForm(){
        form.style.cssText = 'display: block;';
        arrow.style.cssText = 'background: url(\'img/arrow-btn_bottom.png\') 25px bottom no-repeat;';
        flagForm = true;
    }

    function hideForm(){
        form.style.cssText = 'display: none;';
        arrow.style.cssText = 'background: url(\'img/arrow-btn_left.png\') 25px bottom no-repeat;';
        flagForm = false;
    }

    openForm.onclick = function(){
        if (flagForm === false) {
            showForm();
        } else {
            hideForm();
        }
    };
    /*end open form*/

    /*Индикация полей*/

    /*Набор индикаторов, которые будут менять цвет*/
    const indicatorName = document.querySelector('.form__input-indicator_name');
    const indicatorTel = document.querySelector('.form__input-indicator_tel');
    const indicatorMail = document.querySelector('.form__input-indicator_email');

    /*Поля, значения которых будут проверяться*/
    const inputName = document.querySelector('.form__input_name');
    const inputTel = document.querySelector('.form__input_tel');
    const inputMail = document.querySelector('.form__input_email');

    const btn = document.querySelector('.form__input_btn');

    inputName.onblur = function(){
        let name = inputName.value;
        if (name === '') {
            indicatorName.classList.add('form__indicator_invalid');
        } else {
            indicatorName.classList.remove('form__indicator_invalid');
            indicatorName.classList.add('form__indicator_access');
        }
    };

    inputTel.onblur = function(){
        let tel = inputTel.value;
        if (tel === '') {
            indicatorTel.classList.add('form__indicator_invalid');
        } else {
            indicatorTel.classList.remove('form__indicator_invalid');
            indicatorTel.classList.add('form__indicator_access');
        }
    };

    inputMail.onblur = function(){
        let mail = inputMail.value;
        if (mail === '') {
            indicatorMail.classList.add('form__indicator_invalid');
        } else {
            indicatorMail.classList.remove('form__indicator_invalid');
            indicatorMail.classList.add('form__indicator_access');
        }
    };

    /*end*/

    /*результат отправки формы*/
        const sendForm = document.querySelector('.form__input_btn');
        const acceptForm = document.querySelector('.form__accepted');
        let flagAcceptForm = false;

    function showFormAccept(){
        acceptForm.style.cssText = 'display: block;';
        flagAcceptForm = true;
    }

    function hideFormAccept(){
        acceptForm.style.cssText = 'display: none;';
        flagAcceptForm = false;
    }

        sendForm.onclick = function(){
            if (flagAcceptForm === false) {
                showFormAccept();
            } else {
                hideFormAccept();
            }
        };
    /*end*/

    /*hamburger*/
    let burger = document.querySelector('.hamburger');
    let burger_spanUp = document.querySelector('.hamburger__span_up');
    let burger_spanMid = document.querySelector('.hamburger__span_middle');
    let burger_spanDown = document.querySelector('.hamburger__span_down');
    let menu = document.querySelector('.header__menu');
    let flag = false;

    function burgerOn(){
        burger_spanUp.style.cssText = 'transform: rotate(45deg) translate(6px,7px); background-color: #f97f05; transition: all 0.125s ease';
        burger_spanMid.style.cssText = 'transform: translateX(-34px); opacity: 0; background-color: #f97f05; transition: all 0.125s ease';
        burger_spanDown.style.cssText = 'transform: rotate(-45deg) translate(7px,-8px); background-color: #f97f05; transition: all 0.125s ease';
        menu.style.cssText = 'display: block;';
        flag = true;
    }

    function burgerOff(){
        burger_spanUp.style.cssText = 'transform: rotate(0deg) translate(0,0); background-color: #43464a; transition: all 0.125s ease';
        burger_spanMid.style.cssText = 'transform: translateX(0); opacity: 1; background-color: #43464a; transition: all 0.125s ease';
        burger_spanDown.style.cssText = 'transform: rotate(0deg) translate(0,0); background-color: #43464a; transition: all 0.125s ease';
        menu.style.cssText = 'display: none;';
        flag = false;
    }

    burger.addEventListener('click', function(){
        if (flag === false) {
            burgerOn();
        } else {
            burgerOff();
        }
    });


    window.onresize = function(){
        let width = window.innerWidth;
        let menu = document.querySelector('.header__menu');

        if (width > 1023) {
            burgerOff();
            menu.style.cssText='display: block;';
        } else if ((width<1024) && (flag!==true)){
            menu.style.cssText='display: none;';
        }
    };
    /*end*/

};