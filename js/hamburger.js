window.onload = function(){

    let burger = document.querySelector('.hamburger');
    let burger_spanUp = document.querySelector('.hamburger__span_up');
    let burger_spanMid = document.querySelector('.hamburger__span_middle');
    let burger_spanDown = document.querySelector('.hamburger__span_down');
    let menu = document.querySelector('.header__menu');
    let flag = false;

    function burgerOn(){
        burger_spanUp.style.cssText = 'transform: rotate(45deg) translate(6px,7px); background-color: #f97f05; transition: all 0.125s ease';
        burger_spanMid.style.cssText = 'transform: translateX(-34px); opacity: 0; background-color: #f97f05; transition: all 0.125s ease';
        burger_spanDown.style.cssText = 'transform: rotate(-45deg) translate(7px,-8px); background-color: #f97f05; transition: all 0.125s ease';
        menu.style.cssText = 'display: block;';
        flag = true;
    }

    function burgerOff(){
        burger_spanUp.style.cssText = 'transform: rotate(0deg) translate(0,0); background-color: #43464a; transition: all 0.125s ease';
        burger_spanMid.style.cssText = 'transform: translateX(0); opacity: 1; background-color: #43464a; transition: all 0.125s ease';
        burger_spanDown.style.cssText = 'transform: rotate(0deg) translate(0,0); background-color: #43464a; transition: all 0.125s ease';
        menu.style.cssText = 'display: none;';
        flag = false;
    }

    burger.addEventListener('click', function(){
        if (flag === false) {
            burgerOn();
        } else {
            burgerOff();
        }
    });


    window.onresize = function(){
        let width = window.innerWidth;
        let menu = document.querySelector('.header__menu');

        if (width > 1023) {
            burgerOff();
            menu.style.cssText='display: block;';
        } else if ((width<1024) && (flag!==true)){
            menu.style.cssText='display: none;';
        }
    };

};