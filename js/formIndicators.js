window.onload = function (){
    /*Набор индикаторов, которые будут менять цвет*/
    const indicatorName = document.querySelector('.form__input-indicator_name');
    const indicatorTel = document.querySelector('.form__input-indicator_tel');
    const indicatorMail = document.querySelector('.form__input-indicator_email');

    /*Поля, значения которых будут проверяться*/
    const inputName = document.querySelector('.form__input_name');
    const inputTel = document.querySelector('.form__input_tel');
    const inputMail = document.querySelector('.form__input_email');

    const btn = document.querySelector('.form__input_btn');

    inputName.onblur = function(){
        let name = inputName.value;
        if (name === '') {
            indicatorName.classList.add('form__indicator_invalid');
        } else {
            indicatorName.classList.remove('form__indicator_invalid');
            indicatorName.classList.add('form__indicator_access');
        }
    };

    inputTel.onblur = function(){
        let tel = inputTel.value;
        if (tel === '') {
            indicatorTel.classList.add('form__indicator_invalid');
        } else {
            indicatorTel.classList.remove('form__indicator_invalid');
            indicatorTel.classList.add('form__indicator_access');
        }
    };

    inputMail.onblur = function(){
        let mail = inputMail.value;
        if (mail === '') {
            indicatorMail.classList.add('form__indicator_invalid');
        } else {
            indicatorMail.classList.remove('form__indicator_invalid');
            indicatorMail.classList.add('form__indicator_access');
        }
    };

};